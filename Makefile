include .env

IMAGE_NAME	= $(COMPOSE_PROJECT_NAME):$(IMAGE_VERSION)
IMAGE_ARCHIVE	= $(REGISTRY_PREFIX)$(COMPOSE_PROJECT_NAME)-$(IMAGE_VERSION).docker-image.tar.gz

all:; @echo "No target specified."

image.build:
	docker-compose build echo

image.save: image.build
	docker save $(REGISTRY_PREFIX)$(IMAGE_NAME) | gzip >$(IMAGE_ARCHIVE)

image.load:
	gzip -dc $(IMAGE_ARCHIVE) | docker load


.PHONY: image.build image.save image.load

