ARG BASE_IMAGE
FROM $BASE_IMAGE

LABEL maintainer="Tatsuo Nakajyo <tnak@nekonaq.com>"

COPY docker-entrypoint /

ENTRYPOINT ["/docker-entrypoint"]
